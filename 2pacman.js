//////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// TODO LIST ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

//        * Right now, the dots get eaten multiple times if the computer is lagging. I have
//          limited the number of points you can get to a max of 1 per dot, but this does not
//          change the fact that the dots spawn as many times as they are  eaten, and it disables
//          the user from eating the same dot twice and getting points for that.
//          - Find a solution to the dots being eaten multiple times on laggy computers.

//        * Remove FPS counter - search for "debug"
//          - Maybe add them as a ALT+key command?

//        * Add Invisble upgrade    (lasts x seconds || lasts until other guy eats it?)
//        * Add Flying upgrade      (lasts x tiles that you fly over)

//		    * Add randomizer where opponents controls become randomized (or opposite or turned 90 degrees)
//        * Add teleportation? (random placed things that teleport you to random tiles)
//        * Add freezer where opponents become frozen solid for a while

//        * Try only updating updateMyself() when position has changed (still update x/y, but only
//          when position has changed).
//          - For the enemies group, they should now get velocity according to their upgrade
//            and move in their direction. They should only be updated if there's an observed
//            change in direction in the database. Then both X/Y and direction should be changed,
//            to make sure that they are position right before moving in a new direction
//          - there's a chance this can reduce lag a lot, but an equal chance it will cause
//            bug for the enemy movements.


//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// The Script ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////


PlayersDB = new Mongo.Collection('players');
ChatMessagesDB = new Meteor.Collection('chat-messages');
DotsDB = new Mongo.Collection('dots');
UpgradeDB = new Mongo.Collection('upgrades');


if (Meteor.isClient) {
  Meteor.startup(function () {

  	// Move focus to the chat input field when hitting enter
    $(document).on('keypress', function (event) {
      // If the chat is visible
      if (Session.get('chatVisible')){
        // If the chat input does not have focus
        if (!document.getElementById('chat-message').matches(':focus')){
          if (event.which == 13) { // 13 is the enter key event
            // Add focus to the chat input field
            document.getElementById("chat-message").focus();
          }

        // If the chat input has focus but is empty
        }else if (document.getElementById('chat-message').matches(':focus') && document.getElementById('chat-message').value==''){
          if (event.which == 13) { // 13 is the enter key event
            // remove focus from the chat input field
            document.getElementById("chat-message").blur();
          }
        }
      }
    });

  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
    

    // publish the data in database
    Meteor.publish('thePlayers', function(){
      return PlayersDB.find()
    });

    Meteor.publish('theUpgrades', function(){
      return UpgradeDB.find()
    });

    Meteor.publish('theDots', function(){
      return DotsDB.find()
    });

    Meteor.publish('theChatMessages', function(){
      return ChatMessagesDB.find()
    });



    // generate the dots in database if they do not exist
    var amountOfDots = 500;
    var countDotsInDatabase = DotsDB.find({}, {"sort": {"index": 1}}).count();

    if (countDotsInDatabase < amountOfDots) {
      for (var i = 0; i < amountOfDots; i++) {

        DotsDB.insert({
          x: 33,
          y: 33,
          index: i,
        });

      }
    }

    // generate the speedy upgrades in database if they do not exist
    var amountOfUpgradeSpeedy = 2;
    var countUpgradeSpeedyInDatabase = UpgradeDB.find({"type": "speedy"}, {"sort": {"index": 1}}).count();

    if (countUpgradeSpeedyInDatabase < amountOfUpgradeSpeedy) {
      for (var i = 0; i < amountOfUpgradeSpeedy; i++) {

        UpgradeDB.insert({
          type: "speedy",
          x: 65,
          y: 33,
          index: i,
        });

      }
    }

    // generate the eater upgrades in database if they do not exist
    var amountOfUpgradeEater = 2;
    var countUpgradeEaterInDatabase = UpgradeDB.find({"type": "eater"}, {"sort": {"index": 1}}).count();

    if (countUpgradeEaterInDatabase < amountOfUpgradeEater) {
      for (var i = 0; i < amountOfUpgradeEater; i++) {

        UpgradeDB.insert({
          type: "eater",
          x: 33,
          y: 65,
          index: i,
        });

      }
    }

  });
}
