

// Initiate the game object
var PacmanGameObj = PacmanGameObj || {};

// Create the user's unique ID & set the userNotPlaying session
MY_ID = amplify.store('playerId') || amplify.store('playerId', Random.id());
Session.set('userNotPlaying', false);
Session.set('showSettings', false);



  //////////////////////////////////////////////////////////////////////
 /////////       Meteor thingeys for the outer template    ////////////
//////////////////////////////////////////////////////////////////////

// Subscribe to the collections when creating the outer template
Template.outer.onCreated(function(){
  // Subscribe to the published documents
  this.subscribe('thePlayers');
  this.subscribe('theUpgrades');
  this.subscribe('theDots');
  this.subscribe('theChatMessages');
});

// Once the inner template has been loaded (AKA The documents retrieved), create the game:
Template.inner.onRendered(function(){

    // create the game
    PacmanGameObj.game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.CANVAS, "gameDiv");


    //  The Google WebFont Loader will look for this object, so create it before loading the script.
    WebFontConfig = {

        //  'active' means all requested fonts have finished loading
        //  We set a 1 second delay before calling 'createText'.
        //  For some reason if we don't the browser cannot render the text the first time it's created.
        active: function() {  },

        //  The Google Fonts we want to load (specify as many as you like in the array)
        google: {
          families: ['Indie+Flower::latin']
        }

    };


      //////////////////////////////////////////////////////////////////////
     //////////////////       This is the boot state    ///////////////////
    //////////////////////////////////////////////////////////////////////

    // The Boot State
    PacmanGameObj.Boot = function(){};
    //setting game configuration and loading the assets for the loading screen
    PacmanGameObj.Boot.prototype = {
      preload: function() {
        //assets we'll use in the loading screen
        this.load.image('logo', 'assets/logo.png');
        this.load.image('preloadbar', 'assets/preloader-bar.png');
      },
      create: function() {

        // Set the scaling of the game
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;

        Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);

        // Enable physics
        this.physics.startSystem(Phaser.Physics.ARCADE);
        
        this.state.start('Preload');
      }
    };

      //////////////////////////////////////////////////////////////////////
     ////////////////       The preloader state is here    ////////////////
    //////////////////////////////////////////////////////////////////////

    PacmanGameObj.Preload = function(){};

     //loading the game assets
    PacmanGameObj.Preload.prototype = {
      preload: function() {

        // Don't pause the game when the window loses focus.
        this.stage.disableVisibilityChange = true;

        //show logo in loading screen
        this.splash = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
        this.splash.anchor.setTo(0.5);

        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY + 128, 'preloadbar');
        this.preloadBar.anchor.setTo(0.5);

        this.load.setPreloadSprite(this.preloadBar);

        // Load the assets
        this.load.image('dot', 'assets/dot.png');
        this.load.image('upgrade-eater', 'assets/upgrade-eater.png');
        this.load.image('upgrade-speedy', 'assets/upgrade-speedy.png');
        this.load.image('tiles', 'assets/tileset.png');
        this.load.spritesheet('pacman', 'assets/pacman.png', 32, 32);
        this.load.spritesheet('pacman-eater', 'assets/pacman-eater.png', 32, 32);
        this.load.spritesheet('pacman-speedy', 'assets/pacman-speedy.png', 32, 32);
        this.load.tilemap('map', 'assets/medium.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js');

        this.load.image('compass', 'assets/compass_rose.png');
        this.load.image('touch_segment', 'assets/touch_segment.png');
        this.load.image('touch', 'assets/touch.png');

      },
      create: function() {
        
        this.state.start('GameStart');

      }
    };



      //////////////////////////////////////////////////////////////////////
     ///////////////       The actual game state is here    ///////////////
    //////////////////////////////////////////////////////////////////////


    PacmanGameObj.GameStart = function (game) {

        this.map = null;
        this.layer = null;
        this.pacman = null;
        this.dots = null;
        this.enemies = null;
        this.upgradeEaters = null;
        this.upgradeSpeedys = null;

        this.otherPlayersInfo = [];
        this.otherPlayersInfoOld = [];
        this.dotsInfo = [];
        this.dotsInfoOld = [];
        this.upgradeEaterInfo = [];
        this.upgradeEaterInfoOld = [];
        this.upgradeSpeedyInfo = [];
        this.upgradeSpeedyInfoOld = [];

        this.safetile = 13;
        this.safetileArray = [];
        this.gridsize = 32;

        this.speedDefault = 160;
        this.speedEater = 170;
        this.speedSpeedy = 200;
        this.speed = this.speedDefault;
        this.threshold = 3;

        this.amountOfDots = 500;
        this.dotJustEaten = null;
        this.amountOfUpgradeEater = 2;
        this.amountOfUpgradeSpeedy = 2;

        this.myScore = 0;
        this.myUsername = "";
        this.myUpgrade = "none";
        this.myUpgradeLastTime = 0;

        this.highScoresArray = [];
        this.highScoresArrayOld = [];
        this.scoreLabels = "";

        this.marker = new Phaser.Point();
        this.turnPoint = new Phaser.Point();

        this.directions = [ null, null, null, null, null ];
        this.opposites = [ Phaser.NONE, Phaser.RIGHT, Phaser.LEFT, Phaser.DOWN, Phaser.UP ];

        this.current = Phaser.NONE;
        this.turning = Phaser.NONE;

    };

    PacmanGameObj.GameStart.prototype = {

      init: function(username) {
        // Add the username from Main Menu to the variable
        this.myUsername = username || "No Userame?!";
        MY_USERNAME = username || "Chat_abuser";
      },

      preload: function () {

        
      },

      create: function () {

        // Add touch controls from plugin
        this.game.touchControl = this.game.plugins.add(Phaser.Plugin.TouchControl);
        this.game.touchControl.inputEnable();
        this.game.touchControl.settings.singleDirection = true;

        // Create the map (object)
        this.map = this.add.tilemap('map');
        this.map.addTilesetImage('tileset', 'tiles');

        this.layer = this.map.createLayer('Pacman');

        //set world dimensions
        this.game.world.setBounds(0, 0, 2464, 1504);

        // Set the camera at random coordinates
        this.game.camera.setPosition(getRandomInt(1, 2464), getRandomInt(1, 1504));

        // Create the array of safe tiles to spawn & to add random Dots to
        this.safetileArray = this.createTileArray(this.safetile);

        // Generate the dots
        this.generateDots();

        // Generate the upgrade where you eat other players (upgrade-eater)
        this.generateUpgradeEater();

        // Generate the speedy upgrade where you run faster (upgrade-speedy)
        this.generateUpgradeSpeedy();

        //  Pacman should collide with everything except the safe tile
        this.map.setCollisionByExclusion([this.safetile], true, this.layer);

        // Create the other players
        this.createExistingPlayers();

        // Create the cursor keys
        this.cursors = this.input.keyboard.createCursorKeys();

        // Show the highscore
        this.showScoreLabels();

        Session.set('userNotPlaying', true);
      },

      checkKeys: function () {

        if (this.cursors.left.isDown && this.current !== Phaser.LEFT || this.game.touchControl.cursors.left && this.current !== Phaser.LEFT)
        {
            this.checkDirection(Phaser.LEFT);
        }
        else if (this.cursors.right.isDown && this.current !== Phaser.RIGHT || this.game.touchControl.cursors.right && this.current !== Phaser.RIGHT)
        {
            this.checkDirection(Phaser.RIGHT);
        }
        else if (this.cursors.up.isDown && this.current !== Phaser.UP || this.game.touchControl.cursors.up && this.current !== Phaser.UP)
        {
            this.checkDirection(Phaser.UP);
        }
        else if (this.cursors.down.isDown && this.current !== Phaser.DOWN || this.game.touchControl.cursors.down && this.current !== Phaser.DOWN)
        {
            this.checkDirection(Phaser.DOWN);
        }
        else
        {
            //  This forces them to hold the key down to turn the corner
            this.turning = Phaser.NONE;
        }

      },

      checkDirection: function (turnTo) {

        if (this.turning === turnTo || this.directions[turnTo] === null || this.directions[turnTo].index !== this.safetile)
        {
            //  Invalid direction if they're already set to turn that way
            //  Or there is no tile there, or the tile isn't index 1 (a floor tile)
            return;
        }

        //  Check if they want to turn around and can
        if (this.current === this.opposites[turnTo])
        {
            this.move(turnTo);
        }
        else
        {
            this.turning = turnTo;

            this.turnPoint.x = (this.marker.x * this.gridsize) + (this.gridsize * 0.5);
            this.turnPoint.y = (this.marker.y * this.gridsize) + (this.gridsize * 0.5);
        }

      },

      turn: function () {

        var cx = Math.floor(this.pacman.x);
        var cy = Math.floor(this.pacman.y);

        //  This needs a threshold, because at high speeds you can't turn because the coordinates skip past
        if (!this.math.fuzzyEqual(cx, this.turnPoint.x, this.threshold) || !this.math.fuzzyEqual(cy, this.turnPoint.y, this.threshold))
        {
            return false;
        }

        //  Grid align before turning
        this.pacman.x = this.turnPoint.x;
        this.pacman.y = this.turnPoint.y;

        this.pacman.body.reset(this.turnPoint.x, this.turnPoint.y);

        this.move(this.turning);

        this.turning = Phaser.NONE;

        return true;

      },

      move: function (direction) {

        var speed = this.speed;

        if (direction === Phaser.LEFT || direction === Phaser.UP)
        {
            speed = -speed;
        }

        if (direction === Phaser.LEFT || direction === Phaser.RIGHT)
        {
            this.pacman.body.velocity.x = speed;
        }
        else
        {
            this.pacman.body.velocity.y = speed;
        }

        if (direction === Phaser.LEFT)
        {
            this.pacman.play('munch-left');
        }
        else if (direction === Phaser.UP)
        {
            this.pacman.play('munch-up');
        }
        else if (direction === Phaser.DOWN)
        {
            this.pacman.play('munch-down');
        }
        else if (direction === Phaser.RIGHT)
        {
            this.pacman.play('munch-right');
        }

        this.current = direction;

      },

      eatDot: function (pacman, dot) {

        // Show the eating animation and return to munch animation afterwards
        if (this.current == Phaser.RIGHT){
            this.pacman.play('eat-right');
            this.pacman.events.onAnimationComplete.add(function(){
                this.pacman.play('munch-right');
            }, this);
        }else if (this.current == Phaser.LEFT){
            this.pacman.play('eat-left');
            this.pacman.events.onAnimationComplete.add(function(){
                this.pacman.play('munch-left');
            }, this);
        }else if (this.current == Phaser.DOWN){
            this.pacman.play('eat-down');
            this.pacman.events.onAnimationComplete.add(function(){
                this.pacman.play('munch-down');
            }, this);
        }

        // Calculate random number
        var rnd = getRandomInt(0, this.safetileArray.length -1);

        // move the dot to new position
        dot.x = this.safetileArray[rnd].worldX + 14;
        dot.y = this.safetileArray[rnd].worldY + 14;

        // Update the dot in the database
        this.updateDot(this.safetileArray[rnd].worldX, this.safetileArray[rnd].worldY, this.dots.getChildIndex(dot));

        // If this dot was already eaten before, don't increment score
        if (this.dots.getChildIndex(dot) != this.dotJustEaten){
        // Increase the user's score
          this.myScore++;
        }

        // Update the dot index in dotJustEaten for excluding it in giving points again
        this.dotJustEaten = this.dots.getChildIndex(dot);

      },

      // Eat the upgrade which is upgrade-eater
      eatUpgradeEater: function(pacman, Eater) {

        // Calculate random number
        var rnd = getRandomInt(0, this.safetileArray.length -1);

        // Move position of the ugpradeEater
        Eater.x = this.safetileArray[rnd].worldX + 11;
        Eater.x = this.safetileArray[rnd].worldY + 11;

        // Update the dot in the database
        this.updateUpgradeEater(this.safetileArray[rnd].worldX, this.safetileArray[rnd].worldY, this.upgradeEaters.getChildIndex(Eater));
        
        // Change the upgrade status of the user in the database
        this.myUpgrade = "eater";
        this.myUpgradeLastTime = Date.now();

        // Add the upgrade to the database
        this.updateMyUpgradeInfo(this.myUpgrade, this.myUpgradeLastTime);

        // check if all positions as upgrade-eater has been taken
        var eaterCount = this.countUpgradedEaters();

        // If the amount of users with eater upgrade are the max amount
        if (eaterCount >= this.amountOfUpgradeEater){
          this.removeUpgradeFromLastPlayerWithEater();
        }

      },

      // Eat the upgrade which is upgrade-speedy
      eatUpgradeSpeedy: function(pacman, Speedy) {

        // Calculate random number
        var rnd = getRandomInt(0, this.safetileArray.length -1);

        // move position of the upgradeSpeedy
        Speedy.x = this.safetileArray[rnd].worldX + 11;
        Speedy.y = this.safetileArray[rnd].worldY + 11;

        // Update the dot in the database
        this.updateUpgradeSpeedy(this.safetileArray[rnd].worldX, this.safetileArray[rnd].worldY, this.upgradeSpeedys.getChildIndex(Speedy));
        
        // Change the upgrade status of the user in the database
        this.myUpgrade = "speedy";
        this.myUpgradeLastTime = Date.now();

        // Add the upgrade to the database
        this.updateMyUpgradeInfo(this.myUpgrade, this.myUpgradeLastTime);

        // check if all positions as upgrade-eater has been taken
        var speedyCount = this.countUpgradedSpeedys();

        // If the amount of users with eater upgrade are the max amount
        if (speedyCount >= this.amountOfUpgradeSpeedy){
          this.removeUpgradeFromLastPlayerWithSpeedy();
        }

      },

      runPacmenCollision: function (pacman, enemy) {

        // If the current user is an Eater and the enemy is NOT an eater, then eat the other user
        if(this.myUpgrade == "eater" && enemy.texture.baseTexture.source.name != 'pacman-eater'){

          // Show the eating animation and return to munch animation afterwards
          if (this.current == Phaser.RIGHT){
            this.pacman.play('eat-right');
            this.pacman.events.onAnimationComplete.add(function(){
              this.pacman.play('munch-right');
            }, this);
          }else if (this.current == Phaser.LEFT){
            this.pacman.play('eat-left');
            this.pacman.events.onAnimationComplete.add(function(){
              this.pacman.play('munch-left');
            }, this);
          }else if (this.current == Phaser.DOWN){
            this.pacman.play('eat-down');
            this.pacman.events.onAnimationComplete.add(function(){
              this.pacman.play('munch-down');
            }, this);
          }

          // destroy the enemy's sprite
          enemy.destroy();

          // Remove the enemy from the database so they will be returned to the MainMenu
          this.killOtherPlayer(enemy._id);

          // Update their score
          // TODO: Add a percentage of the eaten user's score instead of fixed number
          this.myScore = this.myScore + 100;
        }

        // If an enemy is an eater, then eat the current user
        
      },

      createTileArray: function(tileToCount){
        // declare the temporary array for this method
        var tempArray = [];
        var i = 0;

        // Go through all tiles that matches the one we want
        while(1){
          // break the loop if no more are found
          if(this.map.searchTileIndex(tileToCount, i, false, this.layer) == null) {
            break;
          }

          // push the tile to the array we return
          tempArray.push(this.map.searchTileIndex(tileToCount, i, false, this.layer));

          i++;
        }

        // return the entire array
        return tempArray;
      },

      // Generate the upgrade-eater upgrade on random locations
      generateUpgradeEater: function() {

        // Find the information on dots in the database
        this.upgradeEaterInfo = this.findUpgradeEaters();

        // Create the dots group
        this.upgradeEaters = this.add.group();

        //enable physics in them
        this.upgradeEaters.enableBody = true;
        this.upgradeEaters.physicsBodyType = Phaser.Physics.ARCADE;

        // Add the eater upgrades to the group
        var Eater;
        for (var i = 0; i < this.upgradeEaterInfo.length; i++) {

          //add sprite
          Eater = this.upgradeEaters.create(this.upgradeEaterInfo[i][1], this.upgradeEaterInfo[i][2], 'upgrade-eater');
                      
        }

        //  The dots will need to be offset by 6px to put them back in the middle of the grid
        this.upgradeEaters.setAll('x', 11, false, false, 1);
        this.upgradeEaters.setAll('y', 11, false, false, 1);

        // Observe changes in the upgrade eaters
        this.observeEaterChanges(this);
      },

      observeEaterChanges: function(thisObj){

        var queryEaters = UpgradeDB.find({"type": "eater"});
        var observeChangesInEaters = queryEaters.observe({

          changed: function(newEaterData, oldEaterData) {

            // Find the Eater
            var Eater = thisObj.upgradeEaters.getChildAt(newEaterData.index);

            // Only update the X/Y coordinate if it has changed
            if (Eater.x != newEaterData.x){
              Eater.x = newEaterData.x;

              //  The upgrade will need to be offset by 3px to put them back in the middle of the grid
              thisObj.upgradeEaters.set(Eater, 'x', 11, false, false, 1);
            }
            if (Eater.y != newEaterData.y){
              Eater.y = newEaterData.y;

              //  The upgrade will need to be offset by 3px to put them back in the middle of the grid
              thisObj.upgradeEaters.set(Eater, 'y', 11, false, false, 1);
            }

          },

        });

      },

      // Generate the upgrade-speedy upgrade on random locations
      generateUpgradeSpeedy: function() {

        // Find the information on dots in the database
        this.upgradeSpeedyInfo = this.findUpgradeSpeedys();

        // Create the dots group
        this.upgradeSpeedys = this.add.group();

        //enable physics in them
        this.upgradeSpeedys.enableBody = true;
        this.upgradeSpeedys.physicsBodyType = Phaser.Physics.ARCADE;

        // Add the upgrade speedy to the group
        var Speedy;
        for (var i = 0; i < this.upgradeSpeedyInfo.length; i++) {

          //add sprite
          Speedy = this.upgradeSpeedys.create(this.upgradeSpeedyInfo[i][1], this.upgradeSpeedyInfo[i][2], 'upgrade-speedy');
                      
        }

        //  The dots will need to be offset by 6px to put them back in the middle of the grid
        this.upgradeSpeedys.setAll('x', 11, false, false, 1);
        this.upgradeSpeedys.setAll('y', 11, false, false, 1);

        this.observeSpeedyChanges(this);
      },

      observeSpeedyChanges: function(thisObj){

        var querySpeedys = UpgradeDB.find({"type": "speedy"});
        var observeChangesInSpeedys = querySpeedys.observe({

          changed: function(newSpeedyData, oldSpeedyData) {

            // Find the Speedy
            var Speedy = thisObj.upgradeSpeedys.getChildAt(newSpeedyData.index);

            // Only update the X/Y coordinate if it has changed
            if (Speedy.x != newSpeedyData.x){
              Speedy.x = newSpeedyData.x;

              //  The upgrade will need to be offset by 3px to put them back in the middle of the grid
              thisObj.upgradeSpeedys.set(Speedy, 'x', 11, false, false, 1);
            }
            if (Speedy.y != newSpeedyData.y){
              Speedy.y = newSpeedyData.y;

              //  The upgrade will need to be offset by 3px to put them back in the middle of the grid
              thisObj.upgradeSpeedys.set(Speedy, 'y', 11, false, false, 1);
            }

          },

        });

      },

      // Generate dots at random locations
      generateDots: function() {

        // Find the information on dots in the database
        this.dotsInfo = this.findDots();

        // Create the dots group
        this.dots = this.add.group();

        //enable physics in them
        this.dots.enableBody = true;
        this.dots.physicsBodyType = Phaser.Physics.ARCADE;

        // Add the dots to the group
        var dot;
        for (var i = 0; i < this.dotsInfo.length; i++) {

          //add sprite
          dot = this.dots.create(this.dotsInfo[i][1], this.dotsInfo[i][2], 'dot');
                      
        }
        

        //  The dots will need to be offset by 6px to put them back in the middle of the grid
        this.dots.setAll('x', 14, false, false, 1);
        this.dots.setAll('y', 14, false, false, 1);

        // Start observing the dots for possible changes here
        this.observeDotsChanges(this);
      },

      observeDotsChanges: function(thisObj){
        var queryDots = DotsDB.find({});
        var observeChangesInDots = queryDots.observe({

          changed: function(newDotData, oldDotData) {

            // Find the dot
            var dot = thisObj.dots.getChildAt(newDotData.index);

            // change the dots coordinates
            dot.x = newDotData.x;
            thisObj.dots.set(dot, 'x', 14, false, false, 1);

            dot.y = newDotData.y;
            thisObj.dots.set(dot, 'y', 14, false, false, 1);
          },

        });
      },

      // Method to position Pacman randomely
      randomPacmanPlacement: function(){

        // Set the users username
        this.myUsername = Session.get('username');

        // generate random number from the tiles array
        var rnd;
        rnd = getRandomInt(0, this.safetileArray.length -1);

        // Add the sprite on a random tile
        this.pacman = this.add.sprite(this.safetileArray[rnd].worldX + 16, this.safetileArray[rnd].worldY + 16, 'pacman', 0);
        this.pacman.anchor.set(0.5);
        this.pacman.animations.add('munch-right', [0, 1, 2, 1], 6, true);
        this.pacman.animations.add('munch-left', [3, 4, 5, 4], 6, true);
        this.pacman.animations.add('munch-up', [9, 10, 11, 10], 6, true);
        this.pacman.animations.add('munch-down', [6, 7, 8, 7], 6, true);
        this.pacman.animations.add('eat-right', [12, 13, 14, 13], 30);
        this.pacman.animations.add('eat-left', [15, 16, 17, 16], 30);
        this.pacman.animations.add('eat-down', [18, 19, 20, 19], 30);

        // begin animation and add physics to the pacman
        this.physics.arcade.enable(this.pacman);
        this.pacman.body.setSize(32, 32, 0, 0);

        // Add name label for the current user if not disabled based on settings
        if (!Session.get("setting1Status")){
          var style = { font: "20px Indie Flower", fill: "#ffffff" };  
          var usernameLabelCurrent = this.game.add.text(0, -24, this.myUsername, style);
          usernameLabelCurrent.anchor.set(0.5);
          this.pacman.addChild(usernameLabelCurrent);
        }

        // Start animation and move Pacman in random direction (TODO only directions he can move)
        // generate radom number for random direction
        var randomPlacement = getRandomInt(0, 3);
        // move pacman in random direction
        if (randomPlacement === 0) {
            this.pacman.play('munch-left');
            this.move(Phaser.LEFT);
        }else if (randomPlacement === 1) {
            this.pacman.play('munch-right');
            this.move(Phaser.RIGHT);
        }else if (randomPlacement === 2) {
            this.pacman.play('munch-up');
            this.move(Phaser.UP);
        }else if (randomPlacement === 3) {
            this.pacman.play('munch-down');
            this.move(Phaser.DOWN);
        }
        

        //the camera will follow the player in the world
        this.game.camera.follow(this.pacman);

        // Add the current player to the database
        this.createMyself(this.myUsername, this.safetileArray[rnd].worldX + 16, this.safetileArray[rnd].worldY + 16, Phaser.LEFT)
      
        // Observe if the current user dies
        this.observeMyDeathAndUpgrade(this);
      },

      observeMyDeathAndUpgrade: function(thisObj){
        var queryMyDeath = PlayersDB.find({ "_id": MY_ID });
        var observeChangesInMyDeath = queryMyDeath.observe({

          removed: function(oldData){

            // Send the current user to the MainMenu 
            Session.set('userNotPlaying', true);

            // Disable the chat message field
            $('#chat-message').attr('disabled');

            // Destroy the current user
            thisObj.pacman.destroy();

          }

        });

        var queryMyUpgrade = PlayersDB.find({"_id": MY_ID}, {"fields": {"upgrade": 1}});
        var observeChangesInMyUpgrade = queryMyUpgrade.observe({

          changed: function(newData, oldData){

            // Change the variables and sprite of the user based on the new upgrade
            if (newData.upgrade == "none"){
              // Change the sprite to the standard sprite
              thisObj.pacman.loadTexture('pacman');

              // Change the animation based on the users direction
              if (thisObj.current == Phaser.LEFT) {
                thisObj.pacman.animations.play('munch-left');
              }else if (thisObj.current == Phaser.RIGHT) {
                thisObj.pacman.animations.play('munch-right');
              }else if (thisObj.current == Phaser.UP) {
                thisObj.pacman.animations.play('munch-up');
              }else if (thisObj.current == Phaser.DOWN) {
                thisObj.pacman.animations.play('munch-down');
              }

              // Change the speed of the current user
              thisObj.speed = thisObj.speedDefault;
              thisObj.myUpgrade = 'none';

            }else if (newData.upgrade == "eater"){
              // Change the sprite to the eater sprite
              thisObj.pacman.loadTexture('pacman-eater');
              
              // Change the animation based on the users direction
              if (thisObj.current == Phaser.LEFT) {
                thisObj.pacman.animations.play('munch-left');
              }else if (thisObj.current == Phaser.RIGHT) {
                thisObj.pacman.animations.play('munch-right');
              }else if (thisObj.current == Phaser.UP) {
                thisObj.pacman.animations.play('munch-up');
              }else if (thisObj.current == Phaser.DOWN) {
                thisObj.pacman.animations.play('munch-down');
              }

              // Change the speed of the current user
              thisObj.speed = thisObj.speedEater;
              thisObj.myUpgrade = 'eater';

            }else if (newData.upgrade == "speedy"){
              // Change the sprite to the eater sprite
              thisObj.pacman.loadTexture('pacman-speedy');
              
              // Change the animation based on the users direction
              if (thisObj.current == Phaser.LEFT) {
                thisObj.pacman.animations.play('munch-left');
              }else if (thisObj.current == Phaser.RIGHT) {
                thisObj.pacman.animations.play('munch-right');
              }else if (thisObj.current == Phaser.UP) {
                thisObj.pacman.animations.play('munch-up');
              }else if (thisObj.current == Phaser.DOWN) {
                thisObj.pacman.animations.play('munch-down');
              }

              // Change the speed of the current user
              thisObj.speed = thisObj.speedSpeedy;
              thisObj.myUpgrade = 'speedy';

            }

          }

        });
      },

      // Create the other players sprites
      createExistingPlayers: function() {

        // Create the enemies group
        this.enemies = this.add.group();
        var enemy = null;

        //enable physics in them
        this.enemies.enableBody = true;
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;

        // array[0][0] = _id, array[0][1] = direction, array[0][2] = x, array[0][3] = y,
        // array[0][4] = score, array[0][5] = username, array[0][6] = upgrade
        this.otherPlayersInfo = this.findExistingPlayers(MY_ID);

        // For each user in the Array, create them
        for (i = 0; i < this.otherPlayersInfo.length; i++) {
          
          // Create the sprite for the enemy
          // If he has an upgrade, then add it to him - otherwise the standard sprite
          if (this.otherPlayersInfo[i][6] == "eater"){
            enemy = this.enemies.create(this.otherPlayersInfo[i][2], this.otherPlayersInfo[i][3], 'pacman-eater');
          }else if (this.otherPlayersInfo[i][6] == "speedy"){
            enemy = this.enemies.create(this.otherPlayersInfo[i][2], this.otherPlayersInfo[i][3], 'pacman-speedy');
          }else{
            enemy = this.enemies.create(this.otherPlayersInfo[i][2], this.otherPlayersInfo[i][3], 'pacman');
          }
          enemy.anchor.set(0.5);
          enemy.animations.add('munch-right', [0, 1, 2, 1], 6, true);
          enemy.animations.add('munch-left', [3, 4, 5, 4], 6, true);
          enemy.animations.add('munch-up', [9, 10, 11, 10], 6, true);
          enemy.animations.add('munch-down', [6, 7, 8, 7], 6, true);
          enemy.animations.add('eat-right', [12, 13, 14, 13], 30);
          enemy.animations.add('eat-left', [15, 16, 17, 16], 30);
          enemy.animations.add('eat-down', [18, 19, 20, 19], 30);

          // Add name label for the current enemy
          var style = { font: "20px Indie Flower", fill: "#ffffff" };  
          var usernameLabelCurrent = this.game.add.text(0, -24, this.otherPlayersInfo[i][5], style);
          usernameLabelCurrent.anchor.set(0.5);
          enemy.username = enemy.addChild(usernameLabelCurrent);

          // Hide the username based on settings
          if (Session.get("setting1Status")){
            enemy.username.kill();
          }
          

          // Change his position to face the right direction
          if(this.otherPlayersInfo[i][1] == 1){
            // LEFT
            enemy.animations.play('munch-left');
          }else if(this.otherPlayersInfo[i][1] == 2){
            // RIGHT
            enemy.animations.play('munch-right');
          }else if(this.otherPlayersInfo[i][1] == 3){
            // UP
            enemy.animations.play('munch-up');
          }else if(this.otherPlayersInfo[i][1] == 4){
            // DOWN
            enemy.animations.play('munch-down');
          }

          // Set the enemy's ID
          enemy._id = this.otherPlayersInfo[i][0];

          // Set the enemy's score
          enemy.score = this.otherPlayersInfo[i][4];
        }

        this.observeEnemyChanges(this);
      
      },

      observeEnemyChanges: function(thisObj){
        var queryEnemies = PlayersDB.find({"_id": {$ne: MY_ID}});
        var observeChangesInEnemies = queryEnemies.observe({

          added: function(newEnemyData){

            var enemy = null;

            // Figure out if there's another enemy with that _id
            var enemyExists = false;
            thisObj.enemies.forEach(function(tempEnemy){
              if(tempEnemy._id == newEnemyData._id){
                enemyExists = true;
              }
            })

            // If the user does not already exist with that _id
            if (!enemyExists){
              // Create the sprite for the enemy
              // If he has an upgrade, then add it to him - otherwise the standard sprite
              if (newEnemyData.upgrade == "eater"){
                enemy = thisObj.enemies.create(newEnemyData.x, newEnemyData.y, 'pacman-eater');
              }else if (newEnemyData.upgrade == "speedy"){
                enemy = thisObj.enemies.create(newEnemyData.x, newEnemyData.y, 'pacman-speedy');
              }else if (newEnemyData.upgrade == "none"){
                enemy = thisObj.enemies.create(newEnemyData.x, newEnemyData.y, 'pacman');
              }
              enemy.anchor.set(0.5);
              enemy.animations.add('munch-right', [0, 1, 2, 1], 6, true);
              enemy.animations.add('munch-left', [3, 4, 5, 4], 6, true);
              enemy.animations.add('munch-up', [9, 10, 11, 10], 6, true);
              enemy.animations.add('munch-down', [6, 7, 8, 7], 6, true);
              enemy.animations.add('eat-right', [12, 13, 14, 13], 30);
              enemy.animations.add('eat-left', [15, 16, 17, 16], 30);
              enemy.animations.add('eat-down', [18, 19, 20, 19], 30);
              
              // Add name label for the current enemy
              var style = { font: "20px Indie Flower", fill: "#ffffff" };  
              var usernameLabelCurrent = PacmanGameObj.game.add.text(0, -24, newEnemyData.username, style);
              usernameLabelCurrent.anchor.set(0.5);
              enemy.username = enemy.addChild(usernameLabelCurrent);

              // Hide the username based on settings
              if (Session.get("setting1Status")){
                enemy.username.kill();
              }

              // Change his position to face the right direction
              if(newEnemyData.direction == 1){
                // LEFT
                enemy.animations.play('munch-left');
              }else if(newEnemyData.direction == 2){
                // RIGHT
                enemy.animations.play('munch-right');
              }else if(newEnemyData.direction == 3){
                // UP
                enemy.animations.play('munch-up');
              }else if(newEnemyData.direction == 4){
                // DOWN
                enemy.animations.play('munch-down');
              }

              // Set the enemy's _id
              enemy._id = newEnemyData._id;

              // Set the enemy's score
              enemy.score = newEnemyData.score;

            }

          },

          changed: function(newEnemyData, oldEnemyData) {

            var enemy = null;

            // Find the enemy with the right _id
            thisObj.enemies.forEach(function(tempEnemy){
              if(tempEnemy._id == newEnemyData._id){
                enemy = tempEnemy;
              }
            })

            // Change the X/Y Coordinates of the enemy
            if (enemy.x != newEnemyData.x){
              enemy.x = newEnemyData.x;
            }
            if (enemy.y != newEnemyData.y){
              enemy.y = newEnemyData.y;
            }

            // Hide / Show usernames based on settings
            if (!Session.get("setting1Status") && !enemy.username.alive){
              enemy.username.revive();
            }else if (Session.get("setting1Status") && enemy.username.alive){
              enemy.username.kill();
            }

            // If the username of the enemy has changed, then update it here
            if (enemy.username.alive && enemy.username.text != newEnemyData.username){
              enemy.username.text = newEnemyData.username;
            }

            // Change the enemy's sprite to an upgrade if they have that upgrade
            if (newEnemyData.upgrade == "eater" && enemy.texture.baseTexture.source.name != 'pacman-eater'){
              enemy.loadTexture('pacman-eater');
              enemy.animations.play('munch-right');
            }else if (newEnemyData.upgrade == "speedy" && enemy.texture.baseTexture.source.name != 'pacman-speedy'){
              enemy.loadTexture('pacman-speedy');
              enemy.animations.play('munch-right');
            }else if(newEnemyData.upgrade == "none" && enemy.texture.baseTexture.source.name != 'pacman'){
              enemy.loadTexture('pacman');
              enemy.animations.play('munch-right');
            }

            // Change his animation to face the right direction
            if(newEnemyData.direction == 1 && enemy.animations.name != 'munch-left' && enemy.animations.name != 'eat-left'){
              // LEFT
              enemy.animations.play('munch-left');
            }else if(newEnemyData.direction == 2 && enemy.animations.name != 'munch-right' && enemy.animations.name != 'eat-right'){
              // RIGHT
              enemy.animations.play('munch-right');
            }else if(newEnemyData.direction == 3 && enemy.animations.name != 'munch-up'){
              // UP
              enemy.animations.play('munch-up');
            }else if(newEnemyData.direction == 4 && enemy.animations.name != 'munch-down' && enemy.animations.name != 'eat-down'){
              // DOWN
              enemy.animations.play('munch-down');
            }

            // Show the eating animation and return to munch animation afterwards
            if (enemy.animations.name == 'munch-right' && enemy.score != newEnemyData.score){
              enemy.play('eat-right');
              enemy.events.onAnimationComplete.add(function(){
                enemy.play('munch-right');
              }, this);
            }else if (enemy.animations.name == 'munch-left' && enemy.score != newEnemyData.score){
              enemy.play('eat-left');
              enemy.events.onAnimationComplete.add(function(){
                enemy.play('munch-left');
              }, this);
            }else if (enemy.animations.name == 'munch-down' && enemy.score != newEnemyData.score){
              enemy.play('eat-down');
              enemy.events.onAnimationComplete.add(function(){
                enemy.play('munch-down');
              }, this);
            }

            // Change the enemy's score
            enemy.score = newEnemyData.score;

          },

          removed: function(oldEnemyData){

            var enemy = null;

            // Find the enemy with the right _id
            thisObj.enemies.forEach(function(tempEnemy){
              if(tempEnemy._id == oldEnemyData._id){
                enemy = tempEnemy;
              }
            })

            if(enemy) {
              enemy.destroy();
            }
          }

        });
      },

      // Check if the Pacman is exiting a door and teleport him to the opposite one.
      borderTeleportation: function(){

        // If the pacman is going out on the right door
        if (this.pacman.x > this.map.widthInPixels){
          this.pacman.x = 0;

        // If the pacman is going out on the left door
        }else if (this.pacman.x < 0){
          this.pacman.x = this.map.widthInPixels;

        // If the pacman is going out on the upper door (-1 because it gives error otherwise?!)
        }else if(this.pacman.y < 0){
          this.pacman.y = this.map.heightInPixels -1;

        // If the pacman is going out on the bottom door (-1 because it gives error otherwise?!)
        }else if(this.pacman.y > this.map.heightInPixels -1){
          this.pacman.y = 0;
        }     

      },

      // Show the scores
      showScoreLabels: function() {

        // find the scores in the database
        this.highScoresArray = this.findScores();

        var text = "";
        var style = { font: "24px Indie Flower", fill: "#fff", align: "left" };

        for (i = 0; i < this.highScoresArray.length; i++) {
          text += this.highScoresArray[i][0] + ": " + this.highScoresArray[i][1]+ "\n";
        }

        this.scoreLabels = this.game.add.text(30, 30, text, style);
        //this.scoreLabels.anchor.x = 1;
        this.scoreLabels.fixedToCamera = true;
      },

      updateHighscore: function() {
        // Show / Hide the scores based on settings
        if (!Session.get("setting1Status") && !this.scoreLabels.alive){
          this.scoreLabels.revive();
        }else if(Session.get("setting1Status") && this.scoreLabels.alive){
          this.scoreLabels.kill();
        }

        // Update the scores if they are shown
        if (this.scoreLabels.alive){
          // find the scores in the database
          this.highScoresArray = this.findScores();

          // If the scores have changed then update them
          if (this.highScoresArray != this.highScoresArrayOld){
            var text = "";

            for (i = 0; i < this.highScoresArray.length; i++) {
              text += this.highScoresArray[i][0] + ": " + this.highScoresArray[i][1]+ "\n";
            }

            // set the new text
            this.scoreLabels.setText(text);

            // Update the old information
            this.highScoresArrayOld = this.highScoresArray;
          }
        }
      },

      avoidRandomCollision: function(){
        
        // make sure that this is not run if the getTileDirection is null
        if (this.map.getTileLeft(this.layer.index, this.marker.x, this.marker.y) && this.map.getTileRight(this.layer.index, this.marker.x, this.marker.y) && this.map.getTileAbove(this.layer.index, this.marker.x, this.marker.y) && this.map.getTileBelow(this.layer.index, this.marker.x, this.marker.y)){
          // If the user is trying to go left
          if (this.current == Phaser.LEFT && this.map.getTileLeft(this.layer.index, this.marker.x, this.marker.y).index == this.safetile){
            this.pacman.body.checkCollision.left = false;
          // If the user is trying to go right
          }else if (this.current == Phaser.RIGHT && this.map.getTileRight(this.layer.index, this.marker.x, this.marker.y).index == this.safetile){
            this.pacman.body.checkCollision.right = false;
          // IF the user is trying to go up
          }else if (this.current == Phaser.UP && this.map.getTileAbove(this.layer.index, this.marker.x, this.marker.y).index == this.safetile){
            this.pacman.body.checkCollision.up = false;
          // If the user is trying to go down
          }else if (this.current == Phaser.DOWN && this.map.getTileBelow(this.layer.index, this.marker.x, this.marker.y).index == this.safetile){
            this.pacman.body.checkCollision.down = false;
          // If any of the checkCollision variables are false, set them back to true
          }else if(!(this.pacman.body.checkCollision.left & this.pacman.body.checkCollision.right & this.pacman.body.checkCollision.up & this.pacman.body.checkCollision.down)){
            this.pacman.body.checkCollision.left = true;
            this.pacman.body.checkCollision.right = true;
            this.pacman.body.checkCollision.up = true;
            this.pacman.body.checkCollision.down = true;
          }
        }
      },

      update: function () {

        // If the user is playing
        if(!Session.get('userNotPlaying')){

          // Teleport the user to the opposite door when exiting one of them.
          this.borderTeleportation();


          // collision between the pacman, walls and dots (also avoid random collision with invisible walls)
          this.physics.arcade.collide(this.pacman, this.layer, this.avoidRandomCollision());
          this.physics.arcade.overlap(this.pacman, this.dots, this.eatDot, null, this);

          // collision between the pacman and upgrades
          this.physics.arcade.overlap(this.pacman, this.upgradeEaters, this.eatUpgradeEater, null, this);
          this.physics.arcade.overlap(this.pacman, this.upgradeSpeedys, this.eatUpgradeSpeedy, null, this);

          // Calculate the grid position of the Pacman
          // TODO: Find a way to use a multiplication instead of dividing with / this.gridsize
          //  - for optimization of FPS/speed
          this.marker.x = this.math.snapToFloor(Math.floor(this.pacman.x), this.gridsize) / this.gridsize;
          this.marker.y = this.math.snapToFloor(Math.floor(this.pacman.y), this.gridsize) / this.gridsize;

          //  Update our grid sensors
          this.directions[1] = this.map.getTileLeft(this.layer.index, this.marker.x, this.marker.y);
          this.directions[2] = this.map.getTileRight(this.layer.index, this.marker.x, this.marker.y);
          this.directions[3] = this.map.getTileAbove(this.layer.index, this.marker.x, this.marker.y);
          this.directions[4] = this.map.getTileBelow(this.layer.index, this.marker.x, this.marker.y);

          // Check if a key is pressed down
          this.checkKeys();

          if (this.turning !== Phaser.NONE)
          {
              this.turn();
          }

          // Send the current user's stats to the database
          this.updateMyself(this.pacman.x, this.pacman.y, this.current, this.myScore);

          // Collision between the user and other users when upgraded
          this.physics.arcade.overlap(this.pacman, this.enemies, this.runPacmenCollision, null, this);

        // Else if the user is not playing, keep track of when he does in the modal
        }else{

          // When the user changes ScrollDelta settings
          if (Session.get("setting2Status") && this.layer.renderSettings.enableScrollDelta == true){
            // Disable ScrollDelta
            this.layer.renderSettings.enableScrollDelta = false;
          }else if (!Session.get("setting2Status") && this.layer.renderSettings.enableScrollDelta == false){
            // Enable ScrollDelta
            this.layer.renderSettings.enableScrollDelta = true;
          }

          // If the user begins playing
          if (Session.get('userSignedIn')){

            // Set the session userSignedIn to false
            Session.set('userSignedIn', false);

            // Place the pacman!
            this.randomPacmanPlacement();

            // Set the userNotPlaying session to false to enable chat and remove modal window
            Session.set('userNotPlaying', false);

            // Set the user's chat username
            MY_USERNAME = this.myUsername;
          }

        }

        // Keep track of the scores
        this.updateHighscore();

      },


      
        //////////////////////////////////////////////////////////////////////
       ////////////       Multiplayer & database functions    ///////////////
      //////////////////////////////////////////////////////////////////////

      createMyself: function(username,x,y,direction){
        Meteor.call('createMyself', MY_ID, username, x, y, direction);
      },

      updateMyself: function(x,y,direction,score){
        // If the user actually exists
        if(PlayersDB.find({ "_id": MY_ID }).count()){
        
          Meteor.call('updateMyself', MY_ID, x, y, direction, score);

        }
      },

      updateMyUpgradeInfo: function(upgrade, lastTime){
        // if the user actually exists
        if (PlayersDB.find({ "_id": MY_ID }).count()) {
        
          Meteor.call('updateMyUpgradeInfo', MY_ID, upgrade, lastTime);

        }
      },

      findUpgradeEaters: function(){
        var tempArray = [];
        var allUpgradesInDatabase = UpgradeDB.find({"type": "eater"}, {"sort": {"index": 1}}).fetch();

        // For each user who are NOT the current user, push their data to array
        allUpgradesInDatabase.forEach(function(upgrade){

          tempArray.push([upgrade._id, upgrade.x, upgrade.y, upgrade.index])
        
        })
        // return array of other players information
        return tempArray;
      },

      updateUpgradeEater: function(x,y,index){
        if(PlayersDB.find({ "_id": MY_ID }).count()){
    
          Meteor.call('updateUpgradeEater', x, y, index);

        }
      },

      countUpgradedEaters: function(){
        var tempCount = PlayersDB.find({ "upgrade": "eater" }).count();
        return tempCount;
      },

      removeUpgradeFromLastPlayerWithEater: function(){
        if(PlayersDB.find({ "_id": MY_ID }).count()){
          // Find the last person upgraded with eater
          var lastPlayerUpgraded = PlayersDB.find({"upgrade": "eater"}, {"sort": {"upgradeTime": 1}, "limit": 1}).fetch();
        
          // Get their _id
          var tempPlayerID = lastPlayerUpgraded[0]["_id"]

          // Update their upgrade status to none
          Meteor.call('removeUpgradeFromLastPlayerWithEater', tempPlayerID);
        }
      },

      findUpgradeSpeedys: function(){
        var tempArray = [];
        var allUpgradesInDatabase = UpgradeDB.find({"type": "speedy"}, {"sort": {"index": 1}}).fetch();

        // For each user who are NOT the current user, push their data to array
        allUpgradesInDatabase.forEach(function(upgrade){

          tempArray.push([upgrade._id, upgrade.x, upgrade.y, upgrade.index])
        
        })
        // return array of other players information
        return tempArray;
      },

      updateUpgradeSpeedy: function(x,y,index){
        if(PlayersDB.find({ "_id": MY_ID }).count()){
    
          Meteor.call('updateUpgradeSpeedy', x, y, index);

        }
      },

      countUpgradedSpeedys: function(){
        var tempCount = PlayersDB.find({ "upgrade": "speedy" }).count();
        return tempCount;
      },

      removeUpgradeFromLastPlayerWithSpeedy: function(){
        if(PlayersDB.find({ "_id": MY_ID }).count()){
          // Find the last person upgraded with eater
          var lastPlayerUpgraded = PlayersDB.find({"upgrade": "speedy"}, {"sort": {"upgradeTime": 1}, "limit": 1}).fetch();
        
          // Get their _id
          var tempPlayerID = lastPlayerUpgraded[0]["_id"]

          // Update their upgrade status to none
          Meteor.call('removeUpgradeFromLastPlayerWithSpeedy', tempPlayerID);
        }
      },

      updateDot: function(x,y,index) {
        if(PlayersDB.find({ "_id": MY_ID }).count()){
    
          Meteor.call('updateDot', x, y, index);

        }
      },

      findExistingPlayers: function(currentPlayerID){
        var tempArray = [];
        var allPlayersInDatabase = PlayersDB.find().fetch();

        // For each user who are NOT the current user, push their data to array
        allPlayersInDatabase.forEach(function(player){

          if(player._id != currentPlayerID){
            tempArray.push([player._id, player.direction, player.x, player.y, player.score, player.username, player.upgrade]);
          }
        
        })
        // return array of other players information
        return tempArray;
      },

      findDots: function(){
        var tempArray = [];
        var allDotsIDatabase = DotsDB.find({}, {"sort": {"index": 1}}).fetch();

        // For each user who are NOT the current user, push their data to array
        allDotsIDatabase.forEach(function(dot){

          tempArray.push([dot._id, dot.x, dot.y, dot.index])
        
        })
        // return array of other players information
        return tempArray;
      },

      findScores: function(){
        var tempArray = [];
        var tempScoresReturn = PlayersDB.find({}, {"sort": {"score": -1}, "fields": {"username": 1, "score": 1}, "limit": 10}).fetch();
      
        tempScoresReturn.forEach(function(score){

          tempArray.push([score.username, score.score])
        
        })

        return tempArray;
      },

      killOtherPlayer: function(id){
        if(PlayersDB.find({ "_id": MY_ID }).count()){

          Meteor.call('killOtherPlayer', id);
        }
      }

    };

      //////////////////////////////////////////////////////////////////////
     ////////////       Add the states and start the game    //////////////
    //////////////////////////////////////////////////////////////////////

    PacmanGameObj.game.state.add('Boot', PacmanGameObj.Boot);
    PacmanGameObj.game.state.add('Preload', PacmanGameObj.Preload);
    PacmanGameObj.game.state.add('GameStart', PacmanGameObj.GameStart);

    // Start the game
    PacmanGameObj.game.state.start('Boot');


      //////////////////////////////////////////////////////////////////////
     ////////       Extras to be run once the game is rendered    /////////
    //////////////////////////////////////////////////////////////////////

    // Make the nice looking scrollbar
    $(".chat-div").niceScroll();

});




  //////////////////////////////////////////////////////////////////////
 ///////////////////////       Functions    ///////////////////////////
//////////////////////////////////////////////////////////////////////

// Create random interger in range
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isUsernameAvailable(username){
  var tempReturn = PlayersDB.find({ "username": username, "_id": {$ne: MY_ID} }).count();

  if (tempReturn > 0){
    return false;
  }else{
    return true;
  }
}









  //////////////////////////////////////////////////////////////////////
 //////////////       Meteor thingeys for the Modal    ////////////////
//////////////////////////////////////////////////////////////////////

Template.modal.helpers({
  'userNotPlaying': function(){
    return Session.get('userNotPlaying');
  },

  'showSettings': function(){
    return Session.get('showSettings');
  }
});

Template.modal.events({

    // What happens when clicking the Start Game button
    'click .rv-vanilla-startGameButton': function(){

      // If the input field is not empty, then start the game
      if(/^[^\'\<\>\%\"]*[a-z0-9][^\'\<\>\%\"]+$/i.test(document.getElementById('username').value)) {

        // Only allow player to enter game if username is available
        if(isUsernameAvailable(document.getElementById('username').value)) {
          
          Session.set('username', document.getElementById('username').value);
          Session.set('userSignedIn', true);

          // Enable the chat message field
          $('#chat-message').removeAttr('disabled');

        }else{
          $('#username').notify('Username already taken', { 
            position:"top left", autoHide:true, autoHideDelay:3000, className:"error" 
          });
        }
      }else{

        $('#username').notify('Invalid username', { 
          position:"top left", autoHide:true, autoHideDelay:3000, className:"error" 
        });
        
      }
    },

    // What happens when clicking the Settings button
    'click .rv-vanilla-settingsGameButton': function(){
        if(Session.get('showSettings') == false) {
            Session.set('showSettings', true);
        }else if(Session.get('showSettings') == true){
            Session.set('showSettings', false);
        }
    },

    // What happens when toggling the hide names & scores switch
    "change #setting1": function (event) {
      Session.set("setting1Status", event.target.checked);
    },

    // What happens when toggling the DeltaScroll switch
    "change #setting2": function (event) {
      Session.set("setting2Status", event.target.checked);
    }
});
