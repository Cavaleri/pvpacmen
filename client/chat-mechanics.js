
MY_USERNAME = "Chat_abuser";

// Set the chat to being visible
Session.set('chatVisible', false);


/**
* Templates
*/

// Return the chat messages
Template.chatmessages.helpers({
  messages: function() {
    // Return the chat messages and sort by insert time
    return ChatMessagesDB.find({}, { sort: { time: 1}});
  }
})

// Autoscroll the messages when more are added
Template.messagesItem.rendered = function () {
  $('#chat-div').scrollTop( $('#chat-div').prop("scrollHeight") );
};

// Add the input to the database when pressing enter on the input field
Template.chatinput.events = {
  'keyup input#chat-message' : function (event) {
    // If the user is playing
    if (!Session.get('userNotPlaying')){
      if (event.which == 13) { // 13 is the enter key event
        // Get the username from the game-mechanics.js script
        var name = MY_USERNAME;
        var message = document.getElementById('chat-message');

        // If the message is not empty
        if (message.value != '') {
          // If the message only contains allowed characters
          if (/^[^\<\>\%]*[a-z0-9][^\<\>\%]+$/i.test(message.value)){
            // Get the current time in UTC
            var d = new Date();
    	      var h = (d.getUTCHours()<10?'0':'') + d.getUTCHours();
    	      var m = (d.getUTCMinutes()<10?'0':'') + d.getUTCMinutes();
    	      var messageTime = h + ':' + m;
     
          
            Meteor.call('insertChatMessage', name, message.value, messageTime, Date.now());

     		    // Remove the text from the chat input and lose focus
            document.getElementById('chat-message').value = '';
            document.getElementById('chat-message').blur();
            message.value = '';
          }else{
            
            $('#chat-message').notify('Invalid message', { 
              position:"top left", autoHide:true, autoHideDelay:3000, className:"error" 
            });

          }
        }
      }
    }
  }
}


// Meteor shit to show and hide the arrows that show and hide the chat
Template.arrows.helpers({
  'chatVisible': function(){
    return Session.get('chatVisible');
  }
});

Template.arrowRight.events({
  'click': function(){
    $('.chat-div').stop().animate({
      right: -360
    }, 400);
    $('.arrow-right').stop().animate({
      right: 0
    }, 400, "swing", function(){
      Session.set('chatVisible', false);
    });
  }
});

Template.arrowLeft.events({
  'click': function(){
    $('.chat-div').stop().animate({
      right: 0
    }, 400);
    $('.arrow-left').stop().animate({
      right: 360
    }, 400, "swing", function(){
      Session.set('chatVisible', true);
    });
  }
});
