

Meteor.methods({

	/////////////////// Create & Update Current Player //////////////////

    'createMyself': function(MY_ID,username,x,y,direction){
      PlayersDB.upsert({

	    _id: MY_ID

	  }, {

	    $set: {

	      username: username,
	      x: x,
	      y: y,
	      direction: direction,
	      score: 0,
	      upgrade: "none",
	      upgradeTime: 0,

	    }

	  });
    },

    'updateMyself': function(MY_ID,x,y,direction,score) {
    	PlayersDB.update({

	      _id: MY_ID

	    }, {

	      $set: {

	        x: x,
	        y: y,
	        direction: direction,
	        score: score,

	      }

	    });
    },

    'updateMyUpgradeInfo': function(MY_ID, upgrade, lastTime){
    	PlayersDB.update({

	      _id: MY_ID

	    }, {

	      $set: {

	        upgrade: upgrade,
	        upgradeTime: lastTime,

	      }

	    });
    },

    ////////////// Create & Update the eater upgrades in the database //////////////

    'updateUpgradeEater': function(x,y,index) {
    	UpgradeDB.update({

	      index: index,
	      type: "eater"

	    }, {

	      $set: {

	        x: x,
	        y: y,

	      }

	    });
    },

    'removeUpgradeFromLastPlayerWithEater': function(tempPlayerID){
    	PlayersDB.update({

	      _id: tempPlayerID

	    }, {

	      $set: {

	        upgrade: "none",

	      }

	    });
    },

    ////////////// Create, Update & Find the eater upgrades in the database //////////////

    'updateUpgradeSpeedy': function(x,y,index){
    	UpgradeDB.update({

	      index: index,
	      type: 'speedy'

	    }, {

	      $set: {

	        x: x,
	        y: y,

	      }

	    });
    },

    'removeUpgradeFromLastPlayerWithSpeedy': function(tempPlayerID) {
    	PlayersDB.update({

	      _id: tempPlayerID

	    }, {

	      $set: {

	        upgrade: "none",

	      }

	    });
    },

    /////////////////// Create & Update the dots in the database //////////////////

    'updateDot': function(x,y,index) {
    	DotsDB.update({

	      index: index

	    }, {

	      $set: {

	        x: x,
	        y: y,

	      }

	    });
    },

    // Eat/Kill the other players when upgraded with upgrade-eater
	// remove them from the database

	'killOtherPlayer': function(enemy_id) {
		PlayersDB.remove({

	      _id: enemy_id

	    });
	},

    // Chat message method - insert a message from the chat to the database
    'insertChatMessage': function(name, message, messageTime, time) {

        // Count amount of messages in database
        var messageCount = ChatMessagesDB.find({}).count();
        if(messageCount > 30){
            // Delete the first message in database if more than x are found
            var lastMessageID = ChatMessagesDB.find({},{"sort": {"time":1}, "limit": 1, "fields": {"_id":1}}).fetch();
            ChatMessagesDB.remove({"_id": lastMessageID[0]["_id"]});
        }

        // Insert the message to the database
        ChatMessagesDB.insert({
          name: name,
          message: message,
          messageTime: messageTime,
          time: time
        });
    }

});
